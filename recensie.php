<?php
    $gegevens = array();
    $foutmelding = '';
    $jsonstring = '';
    $berichten = '';

    if(isset($_POST['naam']) == true){
        $gegevens['naam'] = htmlentities($_POST['naam']);
        $gegevens['email'] = htmlentities($_POST['email']);
        $gegevens['recensie'] = htmlentities($_POST['recensie']);


            if(filesize('recensies.txt') > 0)
            {
                $jsonstring = ',';
            }
        $jsonstring .= json_encode($gegevens);
        $handle = fopen('recensies.txt', 'a');
        fwrite($handle, $jsonstring);
        fclose($handle);
        clearstatcache();
        }

if(filesize('recensies.txt') > 0)
{

    $handle = fopen('recensies.txt', 'r');
    $jsonrecensies = fread($handle, filesize('recensies.txt'));
    fclose($handle);
    $berichten = json_decode('[' . $jsonrecensies . ']', true);
}
?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>Recenties</title>
        <link rel="stylesheet" href="styles.css" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> </head>

    <body>
        <div id="form">
            <nav>
                <ul>
                    <li><a href="home.html">Home </a> </li>
                    <li><a href="boeking.html">| Boeking</a></li>
                    <li>| Enquete </li>
                    <li>| Contact </li>
                    <li>| Video </li>
                    <li><a href="muziek.html">| Audio</a></li>
                    <li><a href="recensie.php">| Recensie</a></li>
                </ul>
                <hr> </nav>
            <h2 class="containertitle">Recensies</h2>
            <form method="POST">
                <fieldset>
                    <legend>
                        <h2>Uw mening</h2></legend>
                    <p class="formulierkleur">Uw naam:</p>
                    <input required type="text" placeholder="Naam" name="naam">
                    <br>
                    <br>
                    <p class="formulierkleur">Uw Email:</p>
                    <input required type="email" placeholder="E-mail" name="email">
                    <br>
                    <br>
                    <p class="formulierkleur">Bericht:</p>
                    <input required type="text" placeholder="Uw mening" name="recensie">
                    <br>
                    <br>
                    <input type="submit" value="Verzenden"> </fieldset>
            </form>
            <div id="recenterecensies">
                <h3>Hieronder enkele recente recensies!</h3>
                <?php
                if(empty($berichten) == false)
                {
                    $reversedarray = array_reverse($berichten);
                    $x = 0;
                    foreach($reversedarray as $key => $inputveld)
                    {
                        if($x < 3){
                      echo "<div class='recensie'><div class='test'>
                      <p>Naam:\t" . $inputveld['naam'] . "</p><p>Email:\t" . $inputveld['email'] . "<hr></p><p>Bericht:<br>" . $inputveld['recensie'] . "</div></div>";
                            $x++;
                    }
                    }
                }
                else{
                    echo "Er zijn momenteel nog geen recensies.";
                }
                ?>
            </div>
        </div>
    </body>

    </html>
